function tinhTienThue() {
	var hoTen = document.getElementById("hotencanhan").value;
	var tongThuNhap = document.getElementById("tongthunhap").value * 1;
	console.log('tongThuNhap: ', tongThuNhap);
	var soNguoiPhuThuoc = document.getElementById("songuoiphuthuoc").value * 1;
	var thuNhapChiuThue = tongThuNhap - 4e6 - (soNguoiPhuThuoc * 16e5);
	console.log('thuNhapChiuThue: ', thuNhapChiuThue);
	if (thuNhapChiuThue <= 6e7) {
		thuNhapChiuThue *= 0.05;
	} else if (thuNhapChiuThue > 6e7 && thuNhapChiuThue <= 12e7) {
		thuNhapChiuThue *= 0.1;
	} else if (thuNhapChiuThue > 12e7 && thuNhapChiuThue <= 21e7) {
		thuNhapChiuThue *= 0.15;
	} else if (thuNhapChiuThue > 21e7 && thuNhapChiuThue <= 384e6) {
		thuNhapChiuThue *= 0.2;
	} else if (thuNhapChiuThue > 384e6 && thuNhapChiuThue <= 624e6) {
		thuNhapChiuThue *= 0.25;
	} else if (thuNhapChiuThue > 624e6 && thuNhapChiuThue <= 96e7) {
		thuNhapChiuThue *= 0.3;
	} else if (thuNhapChiuThue > 96e7) {
		thuNhapChiuThue *= 0.35;
	}
	thuNhapChiuThue = new Intl.NumberFormat("vn-VN").format(thuNhapChiuThue);
	document.getElementById('result2').innerHTML = `Họ tên:${hoTen};Tiền thuế thu nhập cá nhập ${thuNhapChiuThue}`;
}